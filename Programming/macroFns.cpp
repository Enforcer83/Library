#include "macroFns.h"

void initSysTick (uint32_t systemClk)
{

    SysTickPeriodSet(systemClk/1000);    // Set the period of the SysTick Timer

    SysTickIntRegister(isrSysTick);     // Specify the Interrupt Service Routine
    SysTickIntEnable();                 // Enable the Interrupt
    SysTickEnable();                    // Enable the SysTick timer

}

void isrSysTick ()
{

    millisCounter++;

}

uint32_t millis ()
{

	return millisCounter;

}
