#include "blinker.h"

Blink::Blink(uint32_t periph, uint32_t port, uint32_t pin, uint32_t on, uint32_t off)
{
		
	ledPort = port;     // Provide the rest of the class with the port information
	ledPin = pin;       // Provide the rest of the class with the pin information
	onTime = on;        // Provide the rest of the class with the on time
	offTime = off;      // Provide the rest of the class with the off time
		
	if (!SysCtlPeripheralReady(periph))
	{
			
        //
		// Enable the peripheral
		//
		SysCtlPeripheralEnable(periph);
			
        //
		// Wait while the peripheral enables.
		//
		while(!SysCtlPeripheralReady(periph))
		{
		}
			
	}
		
    //
	// Configure the pin on the specified port as an output
	//
	GPIOPinTypeGPIOOutput(ledPort, ledPin);
		
	stateOn = false;        // Set the initial state
		
	previousMillis = 0;     // Set the previous time recorded to 0
		
}
	
void Blink::Update()
{
		
	uint32_t currentMillis = millis();
		
	if (stateOn && ((currentMillis - previousMillis) >= onTime)) {
			
		GPIOPinWrite(ledPort, ledPin, 0);
			
		previousMillis = currentMillis;
			
		stateOn = false;
			
	}
		
	else if (!stateOn && ((currentMillis - previousMillis) >= offTime)) {
			
		GPIOPinWrite(ledPort, ledPin, ledPin);
			
		previousMillis = currentMillis;
			
		stateOn = true;
			
	}
		
}
