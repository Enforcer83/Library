#ifndef _MCP23017_
#define _MCP23017_

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/i2c.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"

#include "macroFns.h"

#define BASE_I2C_DEV_ADDR   0x20

enum port
{

    A = 0,
    B

};

enum pins
{

    GPA0 = 0,
    GPA1,
    GPA2,
    GPA3,
    GPA4,
    GPA5,
    GPA6,
    GPA7,
    GPB0,
    GPB1,
    GPB2,
    GPB3,
    GPB4,
    GPB5,
    GPB6,
    GPB7

};

enum intMode
{
    
    CHANGE = 0,
    FALLING,
    RISING
    
};

enum MCP23017Reg
{

    IODIR_A     = 0x00,
    IODIR_B     = 0x01,
    IPOL_A      = 0x02,
    IPOL_B      = 0x03,
    GPINTEN_A   = 0x04,
    GPINTEN_B   = 0x05,
    DEFVAL_A    = 0x06,
    DEFVAL_B    = 0x07,
    INTCON_A    = 0x08,
    INTCON_B    = 0x09,
    IOCON       = 0x0A,
    GPPU_A      = 0x0C,
    GPPU_B      = 0x0D,
    INTF_A      = 0x0E,
    INTF_B      = 0x0F,
    INTCAP_A    = 0x10,
    INTCAP_B    = 0x11,
    GPIO_A      = 0x12,
    GPIO_B      = 0x13,
    OLAT_A      = 0x14,
    OLAT_B      = 0x15,

};

class MCP23017
{

private:

    uint32_t _systemClk;
    uint32_t _periph;
    uint32_t _periphGPIO;
    uint32_t _pinGPIOSDA;
    uint32_t _pinGPIOSCL;
    uint32_t _pinI2CSCL;
    uint32_t _pinI2CSDA;
    uint32_t _baseAddrGPIO;
    uint32_t _baseAddrI2C;
    uint8_t _addr;
    bool _mirror;

public:

    MCP23017(uint32_t systemClk, uint32_t periph, uint32_t periphGPIO,
                uint32_t baseAddrGPIO, uint32_t baseAddrI2C, uint32_t pinGPIOSDA,
                uint32_t pinGPIOSCL, uint32_t pinI2CSDA, uint32_t pinI2CSCL,
                uint8_t address);
    MCP23017(uint32_t systemClk, uint32_t periph, uint32_t periphGPIO,
                uint32_t baseAddrGPIO, uint32_t baseAddrI2C, uint32_t pinGPIOSDA,
                uint32_t pinGPIOSCL, uint32_t pinI2CSDA, uint32_t pinI2CSCL);
    ~MCP23017();

    void begin();
    void portMode(port curPort, uint8_t portPol, uint8_t portDir, uint8_t portPU);
    void pinMode(pins curPin, uint8_t pinPol, uint8_t pinDir, uint8_t pinPU);
    void digitalWrite();
    uint8_t digitalRead();
    void writePort(port wrPort, uint8_t data);
    void write(uint8_t dataA, uint8_t dataB);
    uint8_t readPort(port rdPort);
    uint16_t read();
    void writeContReg(uint8_t reg, uint8_t data);
    uint8_t readContReg(uint8_t reg);

    void interruptConfig(port intPort, pins intPins, intMode mode, bool mirror);
    void disableInterrupt(port intPort, pins intPins);
    void enableInterrupt(port intPort, pins intPins);
    void clearInterrupt(port intPort, pins intPins);

};

#endif
