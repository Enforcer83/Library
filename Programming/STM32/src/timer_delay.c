//***************************************************************************************
//
// Timer based microsecond and millisecond delay library
//
// Based upon the Timer_Delay library developed by Khaled Magdy
// (https://deepbluembedded.com/stm32-delay-microsecond-millisecond-utility-dwt-delay-timer-delay/)
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

#include "timer_delay.h"

void TimerDelayInit(void)
{
    
    TICKS = (HAL_RCC_GetHCLKFreq()/1000000);
    
    HTIMx.Instance = TIMER;
    
    TIM_ClockConfigTypeDef sClockSourceConfig = {0};
    TIM_MasterConfigTypeDef sMasterConfig = {0};

    HTIMx.Init.Prescaler = TICKS - 1;
    HTIMx.Init.CounterMode = TIM_COUNTERMODE_UP;
    HTIMx.Init.Period = 65535;
    HTIMx.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    HTIMx.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
    
    if (HAL_TIM_Base_Init(&HTIMx) != HAL_OK)
    {
        
        Error_Handler();
        
    }
    
    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    
    if (HAL_TIM_ConfigClockSource(&HTIMx, &sClockSourceConfig) != HAL_OK)
    {
        
        Error_Handler();
        
    }
    
    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    
    if (HAL_TIMEx_MasterConfigSynchronization(&HTIMx, &sMasterConfig) != HAL_OK)
    {
        
        Error_Handler();
        
    }

    HAL_TIM_Base_Start(&HTIMx);
    
}

void delay_us(uint16_t micros)
{
    
    HTIMx.Instance->CNT = 0;
	while (HTIMx.Instance->CNT < micros);
    
}

void delay_ms(uint16_t millis)
{
    
    while(millis > 0)
	{
        
		HTIMx.Instance->CNT = 0;
		millis--;
		while (HTIMx.Instance->CNT < 1000);
        
	}
    
}
