//***************************************************************************************
//
// ADS131M04 4-Channel, Simultaneously-Sampling, 24-Bit, Delta-Sigma ADC Library
// Copyright (C) 2024 Jacob Putz, Integrated Microsystem Electronics, LLC
//
//---------------------------------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

#include "ADS131M04.h"

//***************************************************************************************
//***************************************************************************************
//
// Initialization functions
//
//***************************************************************************************
//***************************************************************************************

//***************************************************************************************
//
// Intialization function when using an external clock source for the ADS131M04
//
//***************************************************************************************
uint8_t ADS131M04_Init_Ext_Clk(ADS131M04* adc, SPI_HandleTypeDef *spiHandle,
                               GPIO_TypeDef *csBankADC, uint16_t csPinADC,
                               GPIO_TypeDef *syncPinBank, uint16_t syncPin,
                               bool externalClkADC = true, GPIO_TypeDef *clkOutPinBank,
                               uint16_t clkOutPin, bool clkEnPolHi,
                               uint32_t extClkFreq = 8192000, uint32_t sampleFreq = 8000)
{
                                   
    //
    // Store interface parameters in struct
    //
    adc->_spiHandle = spiHandle;
    adc->_csBankADC = csBankADC;
    adc->_syncPinBank = syncPinBank;
    adc->_clkOutPinBank = clkOutPinBank;
    adc->_csPinADC = csPinADC;
    adc->_syncPin = syncPin;
    adc->_clkOutPin = clkOutPin;
    adc->_externalClkADC = externalClkADC
    adc->_clkFreqADC = extClkFreq;
    adc->_reqSampleFreq = sampleFreq;
    
    // Clear DMA flags
	adc->readingADS = 0;
    
    //
    // If the external clock parameter is set to false then the wrong function was called
    //
    if (!adc->_externalClkADC) {
        
        Error_Handler();
        
    }
    
    uint8_t status = 0;
    uint16_t chipID = 0;
    uint16_t txBuf[10] = {0}, rxBuf[10] = {0};
    uint16_t clkRegVal = 0x0F02; // 0b0000111100000010
    uint32_t actOSR = 512;
    
    //
    // Ensure pins are in a known state
    //
    // Chip Select and Sync/Reset pins are active low so they must be set high.
    // Depending on the external clock source, the enable pin will be either hig or
    // low so a polarity must be specified (bool clkEnPolHi) to determine the enable pins
    // initial state.
    //
    HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_SET);
    HAL_GPIO_WritePin(adc->_syncPinBank, adc->_syncPin, GPIO_PIN_SET);
    
    if (clkEnPolHi) {
        
        HAL_GPIO_WritePin(adc->_clkOutPinBank, adc->_clkOutPin, GPIO_PIN_RESET);
        adc->_clkEnAssert = false;
        
    }
    
    else {
        
        HAL_GPIO_WritePin(adc->_clkOutPinBank, adc->_clkOutPin, GPIO_PIN_SET);
        adc->_clkEnAssert = false;
        
    }
      
    //
    // Disable the external clock
    //
    HAL_GPIO_TogglePin(adc->_clkOutPinBank, adc->_clkOutPin);
    adc->_clkEnAssert = false;    
    
    //
    // Verify the correct chip is connected
    //    
    txBuf[0] = ADS_CMD_RESET;
    
	// Perform soft reset
	HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_RESET);
	status = (HAL_SPI_TransmitReceive(adc->_spiHandle, txBuf, rxBuf, 8, HAL_MAX_DELAY) == HAL_OK);
	HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_SET);
    
	HAL_Delay(250);
    
    // Verify we are communicating with the correct device    
    status += readRegister(ADS_REG_ID, &chipID);
    
    if ((chipID & 0xFF00) != ADS131M04_ID) {
        
        return 0;
        
    }
    
    //
    // MODE Register
    //
    // Configure the MODE register so no register map or SPI input CRC is generated,
    // clear the reset status in the STATUS register, Change the data word length to
    // 32-bits MSB sign extended, SPI timeout is enabled, DRDY output is the logical or
    // of all channels, is high when no data is available and low when data is available.
    //
    // The above results in a 16-bit data word of 0x0314 in hex or 0b0000001100010100 in
    // binary.
    //
    // Datasheet pages 50 and 51
    //
    status += writeRegister(ADS_REG_MODE, 0x0314);
    adc->dataBitMode32 = true;
    adc->signExtend = true;
    adc->crcEnabled = false;
    
    //
    // If the sampling frequency or input clock frequency are not equal to 8kHz and
    // 8.192MHz, respectively, an new Over Sampling Ratio (OSR) will need to be found
    //
    // fs = fmod / OSR = fclkin / (2 * OSR)
    //
    // fs     - sampling frequencing
    // fmod   - modulator frequency
    // fclkin - input clock frequency
    //
    if (adc->_regSampleFreq != 8000 || adc->_clkFreqADC != 8192000) {
        
        actOSR = adc->_clkFreqADC / ( 2 * adc->_regSampleFreq);.
        
        if (actOSR < 96) {
            
            // OSR = 64
            clkRegVal |= (0x0008 << 2);
            adc->_sampleFreq = adc->_clkFreqADC / (2 * 64);
            
        }
        
        else if (actOSR > 192 && actOSR <= 384) {
            
            // OSR = 256
            clkRegVal |= (0x0001 << 2);
            adc->_sampleFreq = adc->_clkFreqADC / (2 * 256);
            
        }
        
        else if (actOSR > 384 && actOSR <= 768) {
            
            // OSR = 512
            clkRegVal |= (0x0002 << 2);
            adc->_sampleFreq = adc->_clkFreqADC / (2 * 512);
            
        }
        
        else if (actOSR > 768 && actOSR <= 1536) {
            
            // OSR = 1024
            clkRegVal |= (0x0003 << 2);
            adc->_sampleFreq = adc->_clkFreqADC / (2 * 1024);
            
        }
        
        else if (actOSR > 1536 && actOSR <= 3072) {
            
            // OSR = 2048
            clkRegVal |= (0x0004 << 2);
            adc->_sampleFreq = adc->_clkFreqADC / (2 * 2048);
            
        }
        
        else if (actOSR > 3072 && actOSR <= 6144) {
            
            // OSR = 4096
            clkRegVal |= (0x0005 << 2);
            adc->_sampleFreq = adc->_clkFreqADC / (2 * 4096);
            
        }
        
        else if (actOSR > 6144 && actOSR <= 12288) {
            
            // OSR = 8192
            clkRegVal |= (0x0006 << 2);
            adc->_sampleFreq = adc->_clkFreqADC / (2 * 8192);
            
        }
        
        else if (actOSR > 12288) {
            
            // OSR = 16256
            clkRegVal |= (0x0007 << 2);
            adc->_sampleFreq = adc->_clkFreqADC / (2 * 16256);
            
        }
        
        else {
            
            adc->_sampleFreq = adc->_clkFreqADC / (2 * 128);
            
        }
        
    }
    
    else {
        
        clkRegVal = 0x0F0A;
        adc->_sampleFreq = adc->_reqSampleFreq;
        
    }
    
    //
    // CLOCK Register
    //
    // Configure the CLOCK register so all ADC channels are enabled, turbo mode is
    // disabled, Over Sampling Ratio (OSR) is 512 and power mode is set to high
    // resolution.  With a clock input of 8.192 MHz this results in a sampling frequency
    // of 8 kHz.
    //
    // fs = fmod / OSR = fclkin / (2 * OSR)
    //
    // The above results in a 16-bit data word of 0x0F0A in hex or 0b0000111100001010 in
    // binary.
    //
    // Datasheet pages 52 and 53
    //
    status += writeRegister(ADS_REG_CLOCK, clkRegVal);
    
    //
    // GAIN Register
    //
    // At reset, all programable gain amplifiers controlled by the GAIN register are set
    // to unity gain (1 V/V).  If a higher gain is needed on any channel, uncomment the
    // writeRegister() command below and provide the gain for the specific channel.  The
    // configuration shown is the default configuration.
    //
    // Datasheet pages 54 and 55
    //
    //status += writeRegister(ADS_REG_GAIN , 0x0000);
    
    //
    // CFG Register
    //
    // At reset, the global chop delay is 16, global chop is disabled, current detection
    // is set for any channel, the number of current detection thresholds exceeded is set
    // to 1, current detect measurements length is set to 128, and current mode detection
    // is disabled.  To change, uncomment the writeRegister() command below and provide
    // the desired configuration.  Utilizing current detection requires setting the 
    // detection threshold.  If current detection is used, immediately set the threshold 
    // using the setThrshReg() command after this function.  The configuration 
    // shown is the default configuration.
    //
    // Datasheet pages 57 and 58
    //
    //status += writeRegister(ADS_REG_CFG , 0x0600);
    
#ifdef (_DWT_DELAY_ || _DWT_DELAY_H_)

    DWT_Delay_Init(void);
    
#endif
    
    return status;
                                   
}

//***************************************************************************************
//
// Intialization function when using a PWM output on the MCU as the ADC clock source
//
//***************************************************************************************


//***************************************************************************************
//***************************************************************************************
//
// Low-level register access functions
//
//***************************************************************************************
//***************************************************************************************

//***************************************************************************************
//
// 
//
//***************************************************************************************
uint16_t readRegister(ADS131M04* adc, uint16_t regAddr) {
    
    if (adc->dataBitMode32) {
        
        uint8_t length = 10;
        
        uint16_t txBuf[10] = {0}, rxBuf[10] = {0};
        
    }
    
    else {
        
        uint8_t length = 8;
        
        uint16_t txBuf[8] = {0}, rxBuf[8] = {0};
        
    }
    
    txBuf[0] = ADS_CMD_RREG | (regAddr << 7);
    
    if (adc->crcEnabled) {
        
        
        
    }
    
    HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(adc->_spiHandle, txBuf, rxBuf, length, HAL_MAX_DELAY);
	HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_SET);
        
    txBuf[0] = ADS_CMD_NULL;
    
    HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(adc->_spiHandle, txBuf, rxBuf, length, HAL_MAX_DELAY);
	HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_SET);
    
    return rxBuf[0];
    
}

//***************************************************************************************
//
// 
//
//***************************************************************************************
uint8_t writeRegister(ADS131M04* adc, uint16_t regAddr, uint16_t data) {
    
    if (adc->dataBitMode32) {
        
        uint8_t length = 10;
        
        uint16_t txBuf[10] = {0}, rxBuf[10] = {0};
        
    }
    
    else {
        
        uint8_t length = 8;
        
        uint16_t txBuf[8] = {0}, rxBuf[8] = {0};
        
    }
    
    txBuf[0] = ADS_CMD_WREG | (regAddr << 7);
    txBuf[1] = data;
    
    if (adc->crcEnabled) {
        
        
        
    }
    
    HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_RESET);
	status = (HAL_SPI_TransmitReceive(adc->_spiHandle, txBuf, rxBuf, length, HAL_MAX_DELAY); == HAL_OK);
	HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_SET);
    
    return status;
    
}

//***************************************************************************************
//
// Cyclic Redundancy Check (CRC) function
//
//***************************************************************************************


//***************************************************************************************
//
// Data Polling function
//
//***************************************************************************************
void getDataADC(ADS131M04* adc) {
    
    bool rxDataGood = true;
    uint16_t crcTemp = 0;
    int32_t tempData[4] = {0};
    
    if (adc->dataBitMode32) {
        
        uint8_t length = 10;
        
        uint16_t txBuf[10] = {0}, rxBuf[10] = {0};
        
    }
    
    else {
        
        uint8_t length = 8;
        
        uint16_t txBuf[8] = {0}, rxBuf[8] = {0};
        
    }
    
    HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(adc->_spiHandle, txBuf, rxBuf, length, HAL_MAX_DELAY);
	HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_SET);
    
    if (adc->crcEnabled) {
        
        // CRC calculation
        
    }
    
    while (!rxDataGood) {
        
        HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_RESET);
        HAL_SPI_TransmitReceive(adc->_spiHandle, txBuf, rxBuf, length, HAL_MAX_DELAY);
        HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_SET);
        
        //CRC calculation
        
    }
    
    if (adc->dataBitMode32) {
        
        if (adc->signExtend) {
            
            adc->resultADC[0] = (int32_t)(((uint32_t)rxBuf[1] << 16) | (uint32_t)rxBuf[2]);
            adc->resultADC[1] = (int32_t)(((uint32_t)rxBuf[3] << 16) | (uint32_t)rxBuf[4]);
            adc->resultADC[2] = (int32_t)(((uint32_t)rxBuf[5] << 16) | (uint32_t)rxBuf[6]);
            adc->resultADC[3] = (int32_t)(((uint32_t)rxBuf[7] << 16) | (uint32_t)rxBuf[8]);
            
        }
        
        else {
            
            adc->resultADC[0] = (int32_t)(((uint32_t)rxBuf[1] << 16) | (uint32_t)rxBuf[2]) >> 8;
            adc->resultADC[1] = (int32_t)(((uint32_t)rxBuf[3] << 16) | (uint32_t)rxBuf[4]) >> 8;
            adc->resultADC[2] = (int32_t)(((uint32_t)rxBuf[5] << 16) | (uint32_t)rxBuf[6]) >> 8;
            adc->resultADC[3] = (int32_t)(((uint32_t)rxBuf[7] << 16) | (uint32_t)rxBuf[8]) >> 8;
            
        }
        
    }
    
    else {
        
        tempData[0] = (int32_t)(((uint32_t)rxBuf[1] << 8) | (((uint32_t)(rxBuf[2] & 0xFF00)) >> 8));
        tempData[1] = (int32_t)((((uint32_t)(rxBuf[2] & 0x00FF)) << 16) | ((uint32_t)rxBuf[3]));
        tempData[2] = (int32_t)(((uint32_t)rxBuf[4] << 8) | (((uint32_t)(rxBuf[5] & 0xFF00)) >> 8));
        tempData[3] = (int32_t)((((uint32_t)(rxBuf[5] & 0x00FF)) << 16) | ((uint32_t)rxBuf[6]));
        
        for (int32_t i = 0; i < 4; i++) {
            
            if (tempData[i] > 8388607) {
                
                adc->resultADC[i] = tempData[i] - 16777216;
                
            }
            
            else {
                
                adc->resultADC[i] = tempData[i];
                
            }
            
        }
        
    }
    
}

//***************************************************************************************
//***************************************************************************************
//
// DMA Access functions
//
//***************************************************************************************
//***************************************************************************************

//***************************************************************************************
//
// DMA data gathering function
//
//***************************************************************************************
void getDataADC_DMA(ADS131M04* adc) {
    
    if (adc->dataBitMode32) {
        
        uint8_t length = 10;

    }
    
    else {
        
        uint8_t length = 8;
        
    }
    
    //
    // Clear the DMA buffers
    //
    adc->txBufDMA = {0,0,0,0,0,0,0,0,0,0};
    adc->rxBufDMA = {0,0,0,0,0,0,0,0,0,0};
    
    HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(adc->_spiHandle, txBufDMA, rxBufDMA, length, HAL_MAX_DELAY);
    
}

//***************************************************************************************
//
// DMA completion callback function
//
//***************************************************************************************
void getDataADC_DMA_Complete(ADS131M04* adc) {
    
    bool rxDataGood = true;
    uint16_t crcTemp = 0;
    int32_t tempData[4] = {0};
    
    if (adc->dataBitMode32) {
        
        uint8_t length = 10;
        
    }
    
    else {
        
        uint8_t length = 8;
        
    }
    
    HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_SET);
    
    if (adc->rcEnabled) {
        
        // CRC calculation
        
    }
    
    while (!rxDataGood) {
        
        HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_RESET);
        HAL_SPI_TransmitReceive(adc->_spiHandle, txBufDMA, rxBufDMA, length, HAL_MAX_DELAY);
        HAL_GPIO_WritePin(adc->_csBankADC, _csPinADC, GPIO_PIN_SET);
        
        //CRC calculation
        
    }
    
    if (adc->dataBitMode32) {
        
        if (adc->signExtend) {
            
            adc->resultADC[0] = (int32_t)(((uint32_t)rxBuf[1] << 16) | (uint32_t)rxBuf[2]);
            adc->resultADC[1] = (int32_t)(((uint32_t)rxBuf[3] << 16) | (uint32_t)rxBuf[4]);
            adc->resultADC[2] = (int32_t)(((uint32_t)rxBuf[5] << 16) | (uint32_t)rxBuf[6]);
            adc->resultADC[3] = (int32_t)(((uint32_t)rxBuf[7] << 16) | (uint32_t)rxBuf[8]);
            
        }
        
        else {
            
            adc->resultADC[0] = (int32_t)(((uint32_t)rxBuf[1] << 16) | (uint32_t)rxBuf[2]) >> 8;
            adc->resultADC[1] = (int32_t)(((uint32_t)rxBuf[3] << 16) | (uint32_t)rxBuf[4]) >> 8;
            adc->resultADC[2] = (int32_t)(((uint32_t)rxBuf[5] << 16) | (uint32_t)rxBuf[6]) >> 8;
            adc->resultADC[3] = (int32_t)(((uint32_t)rxBuf[7] << 16) | (uint32_t)rxBuf[8]) >> 8;
            
        }
        
    }
    
    else {
        
        tempData[0] = (int32_t)(((uint32_t)rxBuf[1] << 8) | (((uint32_t)(rxBuf[2] & 0xFF00)) >> 8));
        tempData[1] = (int32_t)((((uint32_t)(rxBuf[2] & 0x00FF)) << 16) | ((uint32_t)rxBuf[3]));
        tempData[2] = (int32_t)(((uint32_t)rxBuf[4] << 8) | (((uint32_t)(rxBuf[5] & 0xFF00)) >> 8));
        tempData[3] = (int32_t)((((uint32_t)(rxBuf[5] & 0x00FF)) << 16) | ((uint32_t)rxBuf[6]));
        
        for (int32_t i = 0; i < 4; i++) {
            
            if (tempData[i] > 8388607) {
                
                adc->resultADC[i] = tempData[i] - 16777216;
                
            }
            
            else {
                
                adc->resultADC[i] = tempData[i];
                
            }
            
        }
        
    }
    
}

//***************************************************************************************
//***************************************************************************************
//
// Hardware control functions
//
//***************************************************************************************
//***************************************************************************************

//***************************************************************************************
//
// Synchronization pulse
//
//***************************************************************************************
void syncADC(ADS131M04* adc) {
    
    HAL_GPIO_WritePin(adc->_syncPinBank, adc->_syncPin, GPIO_PIN_RESET);
    
#ifdef (_DWT_DELAY_ || _DWT_DELAY_H_)

    DWT_Delay_us(100);
    
#else

    
    
#endif

    HAL_GPIO_WritePin(adc->_syncPinBank, adc->_syncPin, GPIO_PIN_SET);
    
}

//***************************************************************************************
//
// Reset pulse
//
//***************************************************************************************
void resetADC_HW(ADS131M04* adc) {
    
    HAL_GPIO_WritePin(adc->_syncPinBank, adc->_syncPin, GPIO_PIN_RESET);
    
#ifdef (_DWT_DELAY_ || _DWT_DELAY_H_)

    DWT_Delay_us(500);
    
#else

    HAL_Delay(1);
    
#endif

    HAL_GPIO_WritePin(adc->_syncPinBank, adc->_syncPin, GPIO_PIN_SET);
    
}

//***************************************************************************************
//
// Software controlled reset
//
//***************************************************************************************
uint8_t resetADC_SW(ADS131M04* adc) {
    
    if (adc->dataBitMode32) {
        
        uint8_t length = 10;
        
        uint16_t txBuf[10] = {0}, rxBuf[10] = {0};
        
    }
    
    else {
        
        uint8_t length = 8;
        
        uint16_t txBuf[8] = {0}, rxBuf[8] = {0};
        
    }
     
    txBuf[0] = ADS_CMD_RESET;
    
	// Perform soft reset
	HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_RESET);
	status = (HAL_SPI_TransmitReceive(adc->_spiHandle, txBuf, rxBuf, length, HAL_MAX_DELAY) == HAL_OK);
	HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_SET);
    
	HAL_Delay(250);
    
    return status;
    
}

#ifdef _DWT_DELAY_

//
// Data Watchpoint Trigger (DWT) Delay Initialization
//
uint32_t DWT_Delay_Init(void) {
    
    /* Disable TRC */
    CoreDebug->DEMCR &= ~CoreDebug_DEMCR_TRCENA_Msk; // ~0x01000000;
    /* Enable TRC */
    CoreDebug->DEMCR |=  CoreDebug_DEMCR_TRCENA_Msk; // 0x01000000;
 
    /* Disable clock cycle counter */
    DWT->CTRL &= ~DWT_CTRL_CYCCNTENA_Msk; //~0x00000001;
    /* Enable  clock cycle counter */
    DWT->CTRL |=  DWT_CTRL_CYCCNTENA_Msk; //0x00000001;
 
    /* Reset the clock cycle counter value */
    DWT->CYCCNT = 0;
 
    /* 3 NO OPERATION instructions */
    __ASM volatile ("NOP");
    __ASM volatile ("NOP");
    __ASM volatile ("NOP");
 
    /* Check if clock cycle counter has started */
    if(DWT->CYCCNT)
    {
       return 0; /*clock cycle counter started*/
    }
    else
    {
      return 1; /*clock cycle counter not started*/
    }
    
}

//
// This Function Provides Delay In Microseconds Using DWT
//
static inline void DWT_Delay_us(volatile uint32_t au32_microseconds) {
    
    uint32_t au32_initial_ticks = DWT->CYCCNT;
    uint32_t au32_ticks = (HAL_RCC_GetHCLKFreq() / 1000000);
    au32_microseconds *= au32_ticks;
    while ((DWT->CYCCNT - au32_initial_ticks) < au32_microseconds-au32_ticks);
    
}

//
// This Function Provides Delay In Microseconds Using DWT
//
static inline void DWT_Delay_ms(volatile uint32_t au32_milliseconds) {
    
    uint32_t au32_initial_ticks = DWT->CYCCNT;
    uint32_t au32_ticks = (HAL_RCC_GetHCLKFreq() / 1000);
    au32_milliseconds *= au32_ticks;
    while ((DWT->CYCCNT - au32_initial_ticks) < au32_milliseconds);
    
}

#endif

//***************************************************************************************
//***************************************************************************************
//
// System Configuration Register Update and Query
//
//***************************************************************************************
//***************************************************************************************

//***************************************************************************************
//
// 
//
//***************************************************************************************
void setGainReg(ADS131M04* adc, uint16_t data) {
    
    writeRegister(ADS_REG_CLOCK, data);
    
}

//***************************************************************************************
//
// 
//
//***************************************************************************************
void getGainReg(ADS131M04* adc, uint16_t *data) {
    
    
    
}

//***************************************************************************************
//
// 
//
//***************************************************************************************
void setChopReg(ADS131M04* adc, uint16_t data) {
    
    
    
}

//***************************************************************************************
//
// 
//
//***************************************************************************************
void getChopReg(ADS131M04* adc, uint16_t *data) {
    
    
    
}

//***************************************************************************************
//
// 
//
//***************************************************************************************
void setClockReg(ADS131M04* adc, uint16_t data) {
    
    
    
}

//***************************************************************************************
//
// 
//
//***************************************************************************************
void getClockReg(ADS131M04* adc, uint16_t *data) {
    
    
    
}

//***************************************************************************************
//
// 
//
//***************************************************************************************
void setModeReg(ADS131M04* adc, uint16_t data) {
    
    
    
}

//***************************************************************************************
//
// 
//
//***************************************************************************************
void getModeReg(ADS131M04* adc, uint16_t *data) {
    
    
    
}

//***************************************************************************************
//
// 
//
//***************************************************************************************
void setCfgReg(ADS131M04* adc, uint16_t data) {
    
    
    
}

//***************************************************************************************
//
// 
//
//***************************************************************************************
void getCfgReg(ADS131M04* adc, uint16_t *data) {
    
    
    
}

//***************************************************************************************
//
// 
//
//***************************************************************************************
void setThrshReg(ADS131M04* adc, uint32_t data) {
    
    
    
}

//***************************************************************************************
//
// 
//
//***************************************************************************************
void getThrshReg(ADS131M04* adc, uint32_t *data) {
    
    
    
}

//***************************************************************************************
//
// 
//
//***************************************************************************************
void setChCfgReg(ADS131M04* adc, uint8_t ch, uint16_t *data, uint16_t len) {
    
    
    
}

//***************************************************************************************
//
// 
//
//***************************************************************************************
void getChCfgReg(ADS131M04* adc, uint8_t ch, uint16_t *data, uint16_t len) {
    
    
    
}
