//***************************************************************************************
//
// Cirrus Logic CS4272 24-Bit, 192 kHz Stereo Audio CODEC Software Control Library
// Copyright (C) 2024 Jacob Putz, Integrated Microsystem Electronics, LLC
//
//---------------------------------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

#include "codec_cs4272.h"

uint8_t CS4272_REG_CONFIG[8] = {0x01,           // Mode 1 Register (DAC uses I2S Interface)
                                0x80,           // DAC Control (Auto Mute)
                                0x79,           // DAC Vol & Mixing Ctrl (A = B, Soft Ramp, aL/bR)
                                0x7F,           // DAC Ch A Vol Cntrl (-127dB)
                                0x7F,           // DAC Ch B Vol Cntrl (-127dB)
                                0x10,           // ADC Control (ADC uses I2S Interface)
                                0x0A,           // Mode 2 Register (Contol Port En, Mute A = Mute B)
                                CCS4272_ID};    // Device ID
                                

uint8_t CS4272_init(CS4272* codec, I2C_HandleTypeDef *i2cHandle, GPIO_TypeDef *nrstPort, 
                    uint16_t nrstPin, uint8_t addr) {
                   
    codec->_i2cHandle = i2cHandle;
    codec->_nrstPort = nrstPort;
    codec->_nrstPin = nrstPin;
    codec->i2cAddr = addr;
    
    HAL_StatusTypeDef i2cStatus;
    uint8_t deviceID = 0x00;
    
    // Hard Reset CODEC
    CS4272_reset(codec);
    
    // Put Codec in Software Control Mode
    i2cStatus = CS4272_RegWrite(codec, CS4272_REG_MODE2, 0x0F);
    
    if (i2cStatus != HAL_OK) {
    
        return 1;
        
    }
    
    // Check device ID
    i2cStatus = CS4272_RegRead(codec, CS4272_REG_CHIPID, &deviceID)
    
    if ((i2cStatus != HAL_OK) || ((deviceID & 0xF0) != CS4272_ID)) {
    
        return 2;
        
    }
    
    int8_t regIndex;
    uint8_t regData;
    
    for (regIndex = CS4272_REG_MODE1; regIndex <= CS4272_REG_MODE2; regIndex++) {
    
        i2cStatus = CS4272_RegWrite(codec, regIndex, CS4272_REG_CONFIG[regIndex - 1]);
        
        if (i2cStatus != HAL_OK) {
    
            return regIndex;
        
        }
        
        i2cStatus = CS4272_RegRead(codec, regIndex, &regData);
        
        if ((i2cStatus != HAL_OK) || (regData != CS4398_REG_CONFIG[regIndex - 1])) {
    
            return regIndex;
        
        }
        
    }
    
    return 0;
                   
}

void CS4272_reset(CS4272* codec) {
                   
    HAL_GPIO_WritePin(codec->_nrstPort, codec->_nrstPin, GPIO_PIN_RESET);
    HAL_Delay(25);
    HAL_GPIO_WritePin(codec->_nrstPort, codec->_nrstPin, GPIO_PIN_SET);
    HAL_Delay(2);
                   
}


HAL_StatusTypeDef CS4272_RegWrite(CS4272* codec, uint8_t regAddr, uint8_t regData) {
                   
    return HAL_I2C_Mem_Write(codec->_i2cHandle, codec->i2cAddr, regAddr, I2C_MEMADD_SIZE_8BIT, &regData, 1, HAL_MAX_DELAY);
                   
}

HAL_StatusTypeDef CS4272_RegRead(CS4272* codec, uint8_t regAddr, uint8_t *regData) {
                   
    return HAL_I2C_Mem_Read(codec->_i2cHandle, codec->i2cAddr, regAddr, I2C_MEMADD_SIZE_8BIT, regData, 1, HAL_MAX_DELAY);
                   
}
