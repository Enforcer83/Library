//***************************************************************************************
//
// Cirrus Logic CS4398 24-Bit, 192 kHz Stereo Audio DAC Software Control Library
// Copyright (C) 2024 Jacob Putz, Integrated Microsystem Electronics, LLC
//
//---------------------------------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

#include "dac_cs4398.h"

uint8_t CS4398_REG_CONFIG[NUM_REG] = {CS4398_ID,    // Chip ID 
                                      0x10,         // Mode Control (I2S Format,
                                                    // Single Speed)
                                      0x89,         // Volume and Mixing (Vol A = B,
                                                    // aL/bR)
                                      0xE2,         // Mute Cntrl (PCM AM, DSD AM, A = B,
                                                    // Active Low Mute)
                                      0xFF,         // Ch. A Vol (-127.5dB)
                                      0xFF,         // Ch. B Vol (-127.5dB)
                                      0xF0,         // Ramp/Filt Cntrl (Soft Ramp on 
                                                    // zero cross, ramp up and down)
                                      0x40,         // Misc. Cntrl (Control Port enabled)
                                      0x00};        // Misc. Cntrl 2 (All DSD functions
                                                    // disabled)
                                

uint8_t CS4398_init(CS4398* dac, I2C_HandleTypeDef *i2cHandle, GPIO_TypeDef *nrstPort, 
                    uint16_t nrstPin, uint8_t addr) {
                   
    dac->_i2cHandle = i2cHandle;
    dac->_nrstPort = nrstPort;
    dac->_nrstPin = nrstPin;
    dac->i2cAddr = addr;
    
    HAL_StatusTypeDef i2cStatus;
    uint8_t deviceID = 0x00;
    
    // Hard Reset DAC
    CS4398_reset(dac);
    
    // Put DAC in Software Control Mode
    i2cStatus = CS4398_RegWrite(dac, CS4398_REG_MISC, 0xE0);
    
    if (i2cStatus != HAL_OK) {
    
        return 1;
        
    }
    
    // Check device ID
    i2cStatus = CS4398_RegRead(dac, CS4398_REG_CHIPID, &deviceID)
    
    if ((i2cStatus != HAL_OK) || ((deviceID & 0xF8) != CS4398_ID)) {
    
        return 2;
        
    }
    
    int8_t regIndex;
    uint8_t regData;
    
    for (regIndex = CS4398_REG_MISC2; regIndex >= CS4398_REG_MODE; regIndex--) {
    
        if (regIndex != CS4398_REG_MISC) {
    
            i2cStatus = CS4398_RegWrite(dac, regIndex, CS4398_REG_CONFIG[regIndex - 1]);
        
            if (i2cStatus != HAL_OK) {
    
                return regIndex;
        
            }
        
            i2cStatus = CS4398_RegRead(dac, regIndex, &regData);
        
            if ((i2cStatus != HAL_OK) || (regData != CS4398_REG_CONFIG[regIndex - 1])) {
    
                return regIndex;
        
            }
            
        }
        
    }
    
    i2cStatus = CS4398_RegWrite(dac, CS4398_REG_MISC, CS4398_REG_CONFIG[CS4398_REG_MISC - 1]);
        
    if (i2cStatus != HAL_OK) {
    
        return regIndex;
        
    }
        
    i2cStatus = CS4398_RegRead(dac, CS4398_REG_MISC, &regData);
        
    if ((i2cStatus != HAL_OK) || (regData != CS4398_REG_CONFIG[CS4398_REG_MISC - 1])){
    
        return CS4398_REG_MISC;
        
    }
    
    return 0;
                   
}

void CS4398_reset(CS4398* dac) {
                   
    HAL_GPIO_WritePin(dac->_nrstPort, dac->_nrstPin, GPIO_PIN_RESET);
    HAL_Delay(25);
    HAL_GPIO_WritePin(dac->_nrstPort, dac->_nrstPin, GPIO_PIN_SET);
    HAL_Delay(2);
                   
}


HAL_StatusTypeDef CS4398_RegWrite(CS4398* dac, uint8_t regAddr, uint8_t regData) {
                   
    return HAL_I2C_Mem_Write(dac->_i2cHandle, dac->i2cAddr, regAddr, I2C_MEMADD_SIZE_8BIT, &regData, 1, HAL_MAX_DELAY);
                   
}

HAL_StatusTypeDef CS4398_RegRead(CS4398* dac, uint8_t regAddr, uint8_t *regData) {
                   
    return HAL_I2C_Mem_Read(dac->_i2cHandle, dac->i2cAddr, regAddr, I2C_MEMADD_SIZE_8BIT, regData, 1, HAL_MAX_DELAY);
                   
}
