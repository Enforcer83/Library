//***************************************************************************************
//
// Cirrus Logic CS4272 24-Bit, 192 kHz Stereo Audio CODEC Software Control Library
// Copyright (C) 2024 Jacob Putz, Integrated Microsystem Electronics, LLC
//
//---------------------------------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

#ifndef _CODEC_CS4272_H_
#define _CODEC_CS4272_H_

#include "stm32h7xx_hal.h"  // Change this line for the processor family in use.

#define CS4272_ID               0x00

// CS4270 registers addresses 
#define CS4272_REG_MODE1        0x01	// Mode Control 1
#define CS4272_REG_DACCTL       0x02	// DAC Control
#define CS4272_REG_DACMIX       0x03	// DAC Volume and Mixing Control
#define CS4272_REG_DACAVOL      0x04	// DAC Channel A Volume Control
#define CS4272_REG_DACBVOL      0x05	// DAC Channel B Volume Control
#define CS4272_REG_ADCCTL       0x06	// ADC Control
#define CS4272_REG_MODE2        0x07	// Mode Control 2
#define CS4272_REG_CHIPID       0x08	// Chip ID

typedef struct {

    // I2C
    I2C_HandleTypeDef *_i2cHandle;
    uint8_t i2cAddr;
    
    // Hardware Reset
    GPIO_TypeDef *_nrstPort;
    uint16_t _nrstPin;

} CS4272;

uint8_t NUM_REG = (CS4272_REG_CHIPID - CS4272_REG_MODE1) + 1;

extern uint8_t CS4272_REG_CONFIG[NUM_REG];

// Functions
uint8_t CS4272_init(CS4272* codec, I2C_HandleTypeDef *i2cHandle, GPIO_TypeDef *nrstPort, 
                   uint16_t nrstPin, uint8_t addr);
void CS4272_reset(CS4272* codec);

HAL_StatusTypeDef CS4272_RegWrite(CS4272* codec, uint8_t regAddr, uint8_t regData);
HAL_StatusTypeDef CS4272_RegRead(CS4272* codec, uint8_t regAddr, uint8_t *regData);

#endif

