//***************************************************************************************
//
// ADS131M04 4-Channel, Simultaneously-Sampling, 24-Bit, Delta-Sigma ADC Library
// Copyright (C) 2024 Jacob Putz, Integrated Microsystem Electronics, LLC
//
//---------------------------------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

#ifndef _ADS131M04_H_
#define _ADS131M04_H_

#include "stm32f4xx_hal.h"  // Change this line for the processor family in use.

//#define USE_DWT                             // Flag to use the DWT delay.  User must
                                            // determine if DWT is avaiable.

#define ADS131M04_ID            0x2400      // When reading the ID register, always "and"
                                            // the received value with 0xFF00 prior to
                                            // comparing the values
   
//***************************************************************************************
//
// Register Defines
//
//***************************************************************************************
#define ADS_REG_ID              0x0000  
#define ADS_REG_STATUS          0x0001
#define ADS_REG_MODE            0x0002
#define ADS_REG_CLOCK           0x0003
#define ADS_REG_GAIN            0x0004
#define ADS_REG_CFG             0x0006
#define ADS_REG_THRSHLD_MSB     0x0007
#define ADS_REG_THRSHLD_LSB     0x0008
#define ADS_REG_CH0_CFG         0x0009
#define ADS_REG_CH0_OCAL_MSB    0x000A
#define ADS_REG_CH0_OCAL_LSB    0x000B
#define ADS_REG_CH0_GCAL_MSB    0x000C
#define ADS_REG_CH0_GCAL_LSB    0x000D
#define ADS_REG_CH1_CFG         0x000E
#define ADS_REG_CH1_OCAL_MSB    0x000F
#define ADS_REG_CH1_OCAL_LSB    0x0010
#define ADS_REG_CH1_GCAL_MSB    0x0011
#define ADS_REG_CH1_GCAL_LSB    0x0012
#define ADS_REG_CH2_CFG         0x0013
#define ADS_REG_CH2_OCAL_MSB    0x0014
#define ADS_REG_CH2_OCAL_LSB    0x0015
#define ADS_REG_CH2_GCAL_MSB    0x0016
#define ADS_REG_CH2_GCAL_LSB    0x0017
#define ADS_REG_CH3_CFG         0x0018
#define ADS_REG_CH3_OCAL_MSB    0x0019
#define ADS_REG_CH3_OCAL_LSB    0x001A
#define ADS_REG_CH3_GCAL_MSB    0x001B
#define ADS_REG_CH3_GCAL_LSB    0x001C
#define ADS_REG_REGMAP_CRC      0x003E

//***************************************************************************************
//
// Command Defines
//
//***************************************************************************************
#define ADS_CMD_NULL            0x0000
#define ADS_CMD_RESET           0x0011
#define ADS_CMD_STDBY           0x0022
#define ADS_CMD_WAKEUP          0x0033
#define ADS_CMD_LOCK            0x0555
#define ADS_CMD_UNLOCK          0x0655
#define ADS_CMD_RREG            0xA000
#define ADS_CMD_WREG            0x6000

#define DUMMY_DATA              ADS_CMD_NULL

typedef struct {

    // SPI
    SPI_HandleTypeDef *_spiHandle;
    GPIO_TypeDef *_csBankADC;
    uint16_t _csPinADC = 0;
    
    // Clock
    GPIO_TypeDef *_clkOutPinBank;
    uint16_t _clkOutPin = 0;
    bool _externalClkADC = true;
    bool _clkEnAssert = false;
    uint32_t _clkFreqADC = 0;
    uint32_t _reqClkFreqADC = 0;
    
    // Syncronization/Reset
    GPIO_TypeDef *_syncPinBank;
    uint16_t _syncPin0 = 0;
    
    // DMA
    uint8_t readingADS = 0;
    uint16_t txBufDMA[10] = {0};
    volatile uint16_t rxBufDMA[10] = {0};
    
    volatile uint32_t resultADC[4] = {0};
    
    bool dataBitMode32 = false;
    bool signExtend = false;
    bool crcEnabled = false;
    bool isANSI_CRC = false;
    bool inputCRC = false;
    bool regMapCRC = false;
    
    uint32_t _regSampleFreq = 0;
    uint32_t _sampleFreq = 0;
    
} ADS131M04;

//
// Initialization functions
//
uint8_t ADS131M04_Init_Ext_Clk(ADS131M04* adc, SPI_HandleTypeDef *spiHandle,
                               GPIO_TypeDef *csBankADC, uint16_t csPinADC,
                               GPIO_TypeDef *syncPinBank, uint16_t syncPin,
                               bool externalClkADC = true, GPIO_TypeDef *clkOutPinBank,
                               uint16_t clkOutPin, bool clkEnPolHi,
                               uint32_t extClkFreq = 8192000, uint32_t sampleFreq = 8000);

// To Do: Write and initialization function like above to use an MCU PWM pin to the ADC
// clock.

//
// Low-level register access fuctions
//
uint16_t readRegister(ADS131M04* adc, uint16_t regAddr);
uint8_t writeRegister(ADS131M04* adc, uint16_t regAddr, uint16_t data);
// To Do: write functions to perform multi register reads and writes.
// To Do: write a CRC function

//
// Polling
//
void getDataADC(ADS131M04* adc);

//
// DMA
//
void getDataADC_DMA(ADS131M04* adc);
void getDataADC_DMA_Complete(ADS131M04* adc);

//
// Hardware Control
//
void syncADC(ADS131M04* adc);
void resetADC_HW(ADS131M04* adc);
uint8_t resetADC_SW(ADS131M04* adc);

#if !defined(_DWT_DELAY_ || _DWT_DELAY_H_) && defined(USE_DWT)
#define _DWT_DELAY_

uint32_t DWT_Delay_Init(void);
static inline void DWT_Delay_us(volatile uint32_t au32_microseconds);
static inline void DWT_Delay_ms(volatile uint32_t au32_milliseconds);

#endif

//
// System Register Update and Query
//
void setGainReg(ADS131M04* adc, uint16_t data);
void getGainReg(ADS131M04* adc, uint16_t *data);
void setChopReg(ADS131M04* adc, uint16_t data);
void getChopReg(ADS131M04* adc, uint16_t *data);
void setClockReg(ADS131M04* adc, uint16_t data);
void getClockReg(ADS131M04* adc, uint16_t *data);
void setModeReg(ADS131M04* adc, uint16_t data);
void getModeReg(ADS131M04* adc, uint16_t *data);
void setCfgReg(ADS131M04* adc, uint16_t data);
void getCfgReg(ADS131M04* adc, uint16_t *data);
void setThrshReg(ADS131M04* adc, uint32_t data);
void getThrshReg(ADS131M04* adc, uint32_t *data);
void setChCfgReg(ADS131M04* adc, uint8_t ch, uint16_t *data, uint16_t len);
void getChCfgReg(ADS131M04* adc, uint8_t ch, uint16_t *data, uint16_t len);

#endif