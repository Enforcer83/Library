//***************************************************************************************
//
// Cirrus Logic CS4398 24-Bit, 192 kHz Stereo Audio DAC Software Control Library
// Copyright (C) 2024 Jacob Putz, Integrated Microsystem Electronics, LLC
//
//---------------------------------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

#ifndef _DAC_CS4398_H_
#define _DAC_CS4398_H_

#include "stm32h7xx_hal.h"  // Change this line for the processor family in use.

#define CS4398_ID               0x70

// CS4398 registers addresses 
#define CS4398_REG_CHIPID       0x01
#define CS4398_REG_MODE         0x02
#define CS4398_REG_MIX          0x03
#define CS4398_REG_MUTE         0x04
#define CS4398_REG_CHA_VOL      0x05
#define CS4398_REG_CHB_VOL      0x06
#define CS4398_REG_FILT         0x07
#define CS4398_REG_MISC         0x08
#define CS4398_REG_MISC2        0x09

typedef struct {

    // I2C
    I2C_HandleTypeDef *_i2cHandle;
    uint8_t i2cAddr;
    
    // Hardware Reset
    GPIO_TypeDef *_nrstPort;
    uint16_t _nrstPin;

} CS4398;

uint8_t NUM_REG = (CS4398_REG_MISC2 - CS4398_REG_CHIPID) + 1;

extern uint8_t CS4398_REG_CONFIG[NUM_REG];

// Functions
uint8_t CS4398_init(CS4398* dac, I2C_HandleTypeDef *i2cHandle, GPIO_TypeDef *nrstPort, 
                   uint16_t nrstPin, uint8_t addr);
void CS4398_reset(CS4398* dac);

HAL_StatusTypeDef CS4398_RegWrite(CS4398* dac, uint8_t regAddr, uint8_t regData);
HAL_StatusTypeDef CS4398_RegRead(CS4398* dac, uint8_t regAddr, uint8_t *regData);

#endif