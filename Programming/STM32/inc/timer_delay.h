//***************************************************************************************
//
// Timer based microsecond and millisecond delay library
//
// Based upon the Timer_Delay library developed by Khaled Magdy
// (https://deepbluembedded.com/stm32-delay-microsecond-millisecond-utility-dwt-delay-timer-delay/)
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

#ifndef _TIMER_DELAY_H_
#define _TIMER_DELAY_H_

#include "stm32f4xx_hal.h"  // Change this line for the processor family in use.

#define HAL_TIM_MODULE_ENABLED

#define TIMER   TIM10

volatile static TIM_HandleTypeDef HTIMx;
volatile static uint32_t TICKS = 0;

void TimerDelayInit(void);
void delay_us(uint16_t micros);
void delay_ms(uint16_t millis);

#endif