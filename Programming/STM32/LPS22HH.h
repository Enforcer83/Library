//***************************************************************************************
//
// LPS22HH Barometric Pressure Sensor Library
// Copyright (C) 2024 Jacob Putz, Integrated Microsystem Electronics, LLC
//
//---------------------------------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

#ifndef _LPS22HH_H_
#define _LPS22HH_H_

#include "stm32f4xx_hal.h"  // Change this line for the processor family in use.

//
// Expected device ID
//
#define LPS22HH_WHOAMI                  0xB3

//
// Register Defines
//
#define LPS22HH_REG_INT_CFG             0x0B
#define LPS22HH_REG_THS_P_L             0x0C
#define LPS22HH_REG_THS_P_H             0X0D
#define LPS22HH_REG_IF_CTRL             0x0E
#define LPS22HH_REG_WHO_AM_I            0x0F
#define LPS22HH_REG_CTRL_REG1           0x10
#define LPS22HH_REG_CTRL_REG2           0x11
#define LPS22HH_REG_CTRL_REG3           0x12
#define LPS22HH_REG_FIFO_CTRL           0x13
#define LPS22HH_REG_FIFO_WTM            0x14
#define LPS22HH_REG_REF_P_L             0x15
#define LPS22HH_REG_REF_P_H             0x16
#define LPS22HH_REG_RPDS_L              0x18
#define LPS22HH_REG_RPDS_H              0x19
#define LPS22HH_REG_INT_SRC             0x24
#define LPS22HH_REG_FIFO_STATUS1        0x25
#define LPS22HH_REG_FIFO_STATUS2        0x26
#define LPS22HH_REG_STATUS              0x27
#define LPS22HH_REG_PRESS_OUT_XL        0x28
#define LPS22HH_REG_PRESS_OUT_L         0x29
#define LPS22HH_REG_PRESS_OUT_H         0x2A
#define LPS22HH_REG_TEMP_OUT_L          0x2B
#define LPS22HH_REG_TEMP_OUT_H          0x2C
#define LPS22HH_REG_FIFO_OUT_PRESS_XL   0x78
#define LPS22HH_REG_FIFO_OUT_PRESS_L    0x79
#define LPS22HH_REG_FIFO_OUT_PRESS_H    0x7A
#define LPS22HH_REG_FIFO_OUT_TEMP_L     0x7B
#define LPS22HH_REG_FIFO_OUT_TEMP_H     0x7C

//
// Communication dummy data for receive operations
//
#define DUMMY_DATA                      0x00

//
// Conversion factors
//
#define HPA_PER_IN_HG                   33.8639f
#define HPA_PER_PSI                     68.947572932f
#define HPA_PER_MM_HG                   1.33322f
#define HPA_PER_BAR                     1000.0f
#define HPA_PER_ATM                     1013.25f

typedef struct {
	
	// SPI
	SPI_HandleTypeDef *_spiHandle;
	GPIO_TypeDef 	  *_csPinBank;
	uint16_t 		   _csPin;
	
    uint8_t dmaRxBuf[6];
        
    // Final Results
    float pressure_hPa;
    float temperature_C;
	
	// Status tracking
    uint8_t reading;
	
} LPS22H;

// System access functions
//
// To Do: Functions to modify data rate, LPF settings, FIFO usage, conversion
//        methods, FIFO reads.
uint8_t init(LPS22H sensor, SPI_HandleTypeDef *spiHandle, GPIO_TypeDef *csPinBank, uint16_t csPin);
void Read(LPS22H sensor);
uint8_t ReadDMA(LPS22H sensor);
void ReadDMA_Complete(LPS22H sensor);

// Data conversion functions
float inHgConv(LPS22H sensor);
float psiConv(LPS22H sensor);
float mmHgConv(LPS22H sensor);
float atmConv(LPS22H sensor);
float barConv(LPS22H sensor);
float convDegF(LPS22H sensor);
	
// Low level register access functions
uint8_t writeRegister(LPS22H sensor, uint8_t reg, uint8_t data);
uint8_t readRegister(LPS22H sensor, uint8_t reg, uint8_t *data);
//uint8_t writeRegisters(LPS22H sensor, uint8_t reg, uint8_t len, uint8_t data);
//uint8_t readRegisters(LPS22H sensor, uint8_t reg, uint8_t len, uint8_t *data);

#endif