//***************************************************************************************
//
// LPS22HH Barometric Pressure Sensor Library
// Copyright (C) 2024 Jacob Putz, Integrated Microsystem Electronics, LLC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************
#include "LPS22HH.h"

LPS22HH::LPS22HH() {}

uint8_t LPS22HH::init(SPI_HandleTypeDef *spiHandle, GPIO_TypeDef *csPinBank, uint16_t csPin) {

    uint8_t status = 0;

	//
    // Store peripheral data
    //
	_spiHandle = spiHandle;
	_csPinBank = csPinBank;
	_csPin	   = csPin;
    
    //
    // Clear measurements
    //
	pressure_hPa  = 0.0f;
	temperature_C = 0.0f;

	//
    // Check device ID
    //
	uint8_t id;

	status += readRegister(LPS22HH_REG_WHO_AM_I, &id);

	//
    // Make sure device ID matches
    //
	if (id != LPS22HH_WHOAMI) {

		return 0;

	}
    
	HAL_Delay(10);
    
    //
    // Disable I3C and I2C communication
    //
	status += writeRegister(LPS22HH_REG_IF_CTRL, 0x03);
    
	HAL_Delay(10);
    
    //
    // Set pressure configuration
    //
    // 10 Hz sampling rate
    //
	status += writeRegister(LPS22HH_REG_CTRL_REG1, 0x20);
    
	HAL_Delay(10);
    
    //
    // Control Register 2 determines if the the memory should be rebooted, the function
    // of the interrupt pin, the automatic incementing of the register index, software
    // reset, low noise mode and triggering a one shot conversion event.
    // The default state is 0x10 (0b00010000) which translates to normal mode (0),
    // interrupt is active high and push-pull (00), register address index increments
    // automatically (1), normal mode (0), low current mode (0), and idle (0)
    //
    
    //
    // Set Interrupt pin configuration
    //
    // Interrupt triggered when data is ready
    //
	status += writeRegister(LPS22HH_REG_CTRL_REG3, 0x04);
    
	HAL_Delay(10);
    
    //
    // Set Conversion and FIFO Modes
    //
	// Enter continuous conversion mode no FIFO
    //
	status += writeRegister(LPS22HH_REG_FIFO_CTRL, 0x02);
    
	HAL_Delay(10);

	return status;

}

void LPS22HH::Read() {

    uint8_t txBuf[6] = {DUMMY_DATA};
	txBuf[0] = LPS22HH_REG_PRESS_OUT_XL | 0x80;

	uint8_t rxBuf[6] = {DUMMY_DATA};

	HAL_GPIO_WritePin(_csPinBank, _csPin, GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(_spiHandle, txBuf, rxBuf, 6, HAL_MAX_DELAY);
	HAL_GPIO_WritePin(_csPinBank, _csPin, GPIO_PIN_SET);

	//
    // Convert raw adc value to real information.
    //
	int32_t pres = ((uint32_t) rxBuf[3] << 16) | ((uint32_t) rxBuf[2] << 8) |
                   ((uint32_t) rxBuf[1]);
	pres = (pres & 1 << 23) ? (0xFF000000 | pres) : pres;

	int16_t temp = ((uint16_t) rxBuf[5] << 8) | ((uint16_t) rxBuf[4]);

	//
    // Convert to readable data
    //
	temperature_C  = (float) temp / 100.0f;
	pressure_hPa = (float) pres / 4096.0f;

}

uint8_t LPS22HH::ReadDMA() {

    uint8_t txBuf[6] = {0x00};
	txBuf[0] = LPS22HH_REG_PRESS_OUT_XL | 0x80;

	HAL_GPIO_WritePin(_csPinBank, _csPin, GPIO_PIN_RESET);
	if (HAL_SPI_TransmitReceive_DMA(_spiHandle, txBuf, (uint8_t *) dmaRxBuf, 6) == HAL_OK) {

		reading = 1;
		return 1;

	} else {

		return 0;

	}

}

void LPS22HH::ReadDMA_Complete() {

    HAL_GPIO_WritePin(_csPinBank, _csPin, GPIO_PIN_SET);
	reading = 0;

	//
    // Convert raw adc value to real information.
    //
	int32_t pres = ((uint32_t) rxBuf[3] << 16) | ((uint32_t) rxBuf[2] << 8) |
                   ((uint32_t) rxBuf[1]);
	pres = (pres & 1 << 23) ? (0xFF000000 | pres) : pres;

	int16_t temp = ((uint16_t) rxBuf[5] << 8) | ((uint16_t) rxBuf[4]);

	//
    // Convert to readable data
    //
	temperature_C  = (float) temp / 100.0f;
	pressure_hPa = (float) pres / 4096.0f;

}

float LPS22HH::inHgConv() {

    return (pressure_hPa / HPA_PER_IN_HG);

}

float LPS22HH::psiConv() {

    return (pressure_hPa / HPA_PER_PSI);

}

float LPS22HH::mmHgConv() {

    return (pressure_hPa / HPA_PER_MM_HG);

}

float LPS22HH::atmConv() {

    return (pressure_hPa / HPA_PER_ATM);

}

float LPS22HH::barConv() {

    return (pressure_hPa / HPA_PER_BAR);

}

float LPS22HH::convDegF() {

    return ((1.8f * temperature_C) + 32.0f);  // ((9/5) * deg C) + 32

}

//
// Low Level register access functions for both single and multi register read and writes
//
uint8_t LPS22HH::writeRegister(uint8_t reg, uint8_t data) {

    uint8_t status = 0;
    uint8_t txBuf[2] = {0x00, 0x00}, rxBuf = {0x00, 0x00};

    txBuf[0] = reg;
	txBuf[1] = data;

	HAL_GPIO_WritePin(_csPinBank, _csPin, GPIO_PIN_RESET);
	status += (HAL_SPI_TransmitReceive(_spiHandle, txBuf, rxBuf, 2, HAL_MAX_DELAY)
                == HAL_OK);
	HAL_GPIO_WritePin(_csPinBank, _csPin, GPIO_PIN_SET);
    
    return status;

}

uint8_t LPS22HH::readRegister(uint8_t reg, uint8_t *data) {

    uint8_t status = 0;
    uint8_t txBuf[2] = {0x00, 0x00}, rxBuf = {0x00, 0x00};

    txBuf[0] = reg | 0x80;

	HAL_GPIO_WritePin(_csPinBank, _csPin, GPIO_PIN_RESET);
	status += (HAL_SPI_TransmitReceive(_spiHandle, txBuf, rxBuf, 2, HAL_MAX_DELAY)
                == HAL_OK);
	HAL_GPIO_WritePin(_csPinBank, _csPin, GPIO_PIN_SET);
    
    &data = rxBuf[1];
    
    return status;

}
/*
uint8_t LPS22HH::writeRegisters(uint8_t reg, uint8_t len, uint8_t data) {

    uint8_t status = 0;
    uint8_t txBuf[2] = {0x00, 0x00}, rxBuf = {0x00, 0x00};

    txBuf[0] = reg;
	txBuf[1] = data;

	HAL_GPIO_WritePin(_csPinBank, _csPin, GPIO_PIN_RESET);
	status += (HAL_SPI_TransmitReceive(_spiHandle, txBuf, rxBuf, 2, HAL_MAX_DELAY)
                == HAL_OK);
	HAL_GPIO_WritePin(_csPinBank, _csPin, GPIO_PIN_SET);
    
    return status;

}

uint8_t LPS22HH::readRegisters(uint8_t reg, uint8_t len, uint8_t *data) {

    uint8_t status = 0;
    uint8_t txBuf[2] = {0x00, 0x00}, rxBuf = {0x00, 0x00};

    txBuf[0] = reg | 0x80;
	txBuf[1] = DUMMY_DATA;

	HAL_GPIO_WritePin(_csPinBank, _csPin, GPIO_PIN_RESET);
	status += (HAL_SPI_TransmitReceive(_spiHandle, txBuf, rxBuf, 2, HAL_MAX_DELAY)
                == HAL_OK);
	HAL_GPIO_WritePin(_csPinBank, _csPin, GPIO_PIN_SET);
    
    &data = rxBuf[1];
    
    return status;

}*/
