#ifndef _MACROS_
#define _MACROS_

#include <stdint.h>
#include <stdbool.h>
#include "driverlib/systick.h"

#define PI				            3.1415926535897932384626433832795
#define HALF_PI				        1.5707963267948966192313216916398
#define TWO_PI				        6.283185307179586476925286766559
#define DEG_TO_RAD			        0.017453292519943295769236907684886
#define RAD_TO_DEG			        57.295779513082320876798154814105
#define EULER				        2.718281828459045235360287471352
#define PYTHAGORAS			        1.41421356237309504880168872421
#define ROOT2				        PYTHAGORAS
#define THEODORUS			        1.732050807568877293527446341506
#define ROOT3				        THEODORUS
#define PHI				            1.618033988749894848204586834366

#ifdef abs
#undef abs
#endif

#define min(a,b)			        ((a)<(b)?(a):(b))
#define max(a,b)			        ((a)>(b)?(a):(b))
#define abs(x)				        ((x)>0?(x):-(x))
#define constrain(amt,low,high) 	((amt)<(low)?(low):((amt)>(high)?(high):(amt)))
#define round(x)			        ((x)>=0?(uint32_t)((x)+0.5):(uint32_t)((x)-0.5))
#define radians(deg)			    ((deg)*DEG_TO_RAD)
#define degrees(rad)			    ((rad)*RAD_TO_DEG)
#define sq(x) ((x)*(x))

#define lowByte(w)			        ((uint8_t) ((w) & 0xff))
#define highByte(w)			        ((uint8_t) ((w) >> 8))

#define lowByte32(w)			    ((uint8_t) ((w) & 0xff))
#define midLowByte32(w)			    ((uint8_t) (((w) >> 8) & 0xff))
#define	midHighByte32(w)		    ((uint8_t) (((w) >> 16) & 0xff))
#define highByte32(w)			    ((uint8_t) ((w) >> 24))

//----------------------------------------------------------------------------
//
// Global holding for millisecond counter.
//
//----------------------------------------------------------------------------
static volatile uint32_t millisCounter = 0;

//----------------------------------------------------------------------------
//
//
//
//----------------------------------------------------------------------------
void initSysTick(uint32_t systemClk);

//----------------------------------------------------------------------------
//
//
//
//----------------------------------------------------------------------------
void isrSysTick();

//----------------------------------------------------------------------------
//
// This function returns the current value of the global millisecond counter 
// variable millisCounter.  This variable is updated whenever the System
// Timer, SysTick, rolls over.
//
//----------------------------------------------------------------------------
uint32_t millis ();

#endif
