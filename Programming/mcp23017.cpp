#include "mcp23017.h"
//  |   |   |   |   |   |
MCP23017::MCP23017(uint32_t systemClk, uint32_t periph, uint32_t periphGPIO,
                    uint32_t baseAddrGPIO, uint32_t baseAddrI2C, uint32_t pinGPIOSDA,
                    uint32_t pinGPIOSCL, uint32_t pinI2CSDA, uint32_t pinI2CSCL,
                    uint8_t address)
{

    _systemClk = systemClk;
    _periph = periph;
    _periphGPIO = periphGPIO;
    _pinGPIOSDA = pinGPIOSDA;
    _pinGPIOSCL = pinGPIOSCL;
    _pinI2CSCL = pinI2CSCL;
    _pinI2CSDA = pinI2CSDA;
    _baseAddrGPIO = baseAddrGPIO;
    _baseAddrI2C = baseAddrI2C;
    _addr = address;

    if (!SysCtlPeripheralReady(_periphGPIO))
    {

        //
        // Enable the GPIO peripheral
        //
        SysCtlPeripheralEnable(_periphGPIO);

        //
        // Wait while the GPIO peripheral enables.
        //
        while(!SysCtlPeripheralReady(_periphGPIO))
        {
        }

    }

    if (!SysCtlPeripheralReady(_periph))
    {

        //
        // Enable the I2C peripheral
        //
        SysCtlPeripheralEnable(_periph);

        //
        // Wait while the I2C peripheral enables.
        //
        while(!SysCtlPeripheralReady(_periph))
        {
        }

        //
        // Configure GPIO Port pins to be used as I2C.
        //
        GPIOPinTypeI2C(_baseAddrGPIO, _pinGPIOSDA);
        GPIOPinTypeI2CSCL(_baseAddrGPIO, _pinGPIOSCL);

        //
        // Enable I2C functionality on GPIO Port.
        //
        GPIOPinConfigure(_pinI2CSCL);
        GPIOPinConfigure(_pinI2CSDA);

    }

}
//  |   |   |   |   |   |
MCP23017::MCP23017(uint32_t systemClk, uint32_t periph, uint32_t periphGPIO,
                    uint32_t baseAddrGPIO, uint32_t baseAddrI2C, uint32_t pinGPIOSDA,
                    uint32_t pinGPIOSCL, uint32_t pinI2CSDA, uint32_t pinI2CSCL)
{

    _systemClk = systemClk;
    _periph = periph;
    _periphGPIO = periphGPIO;
    _pinGPIOSDA = pinGPIOSDA;
    _pinGPIOSCL = pinGPIOSCL;
    _pinI2CSCL = pinI2CSCL;
    _pinI2CSDA = pinI2CSDA;
    _baseAddrGPIO = baseAddrGPIO;
    _baseAddrI2C = baseAddrI2C;
    _addr = BASE_I2C_DEV_ADDR;

    if (!SysCtlPeripheralReady(_periphGPIO))
    {

        //
        // Enable the GPIO peripheral
        //
        SysCtlPeripheralEnable(_periphGPIO);

        //
        // Wait while the GPIO peripheral enables.
        //
        while(!SysCtlPeripheralReady(_periphGPIO))
        {
        }

    }

    if (!SysCtlPeripheralReady(_periph))
    {

        //
        // Enable the I2C peripheral
        //
        SysCtlPeripheralEnable(_periph);

        //
        // Wait while the I2C peripheral enables.
        //
        while(!SysCtlPeripheralReady(_periph))
        {
        }

        //
        // Configure GPIO Port pins to be used as I2C.
        //
        GPIOPinTypeI2C(_baseAddrGPIO, _pinGPIOSDA);
        GPIOPinTypeI2CSCL(_baseAddrGPIO, _pinGPIOSCL);

        //
        // Enable I2C functionality on GPIO Port.
        //
        GPIOPinConfigure(_pinI2CSCL);
        GPIOPinConfigure(_pinI2CSDA);

    }

}

MCP23017::~MCP23017()
{



}

void MCP23017::begin()
{

    I2CMasterInitExpClk(_baseAddrI2C, _systemClk, true);

    I2CMasterSlaveAddrSet(_baseAddrI2C, _addr, true);

    //
    // Check data at Address 0x10 to determine if device is in bank mode.
    //
    I2CMasterDataPut(_baseAddrI2C, 0x10);

    //
    // If data is not equal to 0, the device is not in bank mode.
    // Place the device in bank mode.
    //
    if (I2CMasterDataGet(_baseAddrI2C) != 0x00)
    {

        I2CMasterSlaveAddrSet(_baseAddrI2C, _addr, false);

        I2CMasterDataPut(_baseAddrI2C, 0x05);
        I2CMasterDataPut(_baseAddrI2C, 0x00);

    }

}

void MCP23017::portMode(port curPort, uint8_t portPol, uint8_t portDir, uint8_t portPU)
{

    if (curPort == A)
    {

        writeContReg(IODIR_A, portDir);
        writeContReg(IPOL_A, portPol);
        writeContReg(GPPU_A, portPU);

    }

    else
    {

        writeContReg(IODIR_B, portDir);
        writeContReg(IPOL_B, portPol);
        writeContReg(GPPU_B, portPU);

    }

}

void MCP23017::pinMode(pins curPin, uint8_t pinPol, uint8_t pinDir, uint8_t pinPU)
{

    uint8_t curPort = 0;

    if (curPin > GPB7)
    {

        curPin = GPB7;

    }

    if (curPin < GPB0)
    {

        curPort = A;

    }

    else
    {

        curPort = B;

    }

    if (curPort == A)
    {
        switch (curPin)
        {

            case GPA0:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            case GPA1:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            case GPA2:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            case GPA3:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            case GPA4:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            case GPA5:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            case GPA6:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            case GPA7:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            default:

                while(1)
                {

                    // Why did I get here?

                }

                break;

        }

    }

    else
    {

        switch (curPin)
        {

            case GPB0:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            case GPB1:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            case GPB2:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            case GPB3:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            case GPB4:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            case GPB5:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            case GPB6:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            case GPB7:

                writeContReg(IODIR_A, (pinDir & curPin));
                writeContReg(IPOL_A, (pinPol & curPin));
                writeContReg(GPPU_A, (pinPU & curPin));

                break;

            default:

                while(1)
                {

                    // Why did I get here?

                }

                break;

        }

    }

}

void MCP23017::digitalWrite()
{



}

uint8_t MCP23017::digitalRead()
{

    return 0;

}

void MCP23017::writePort(port wrPort, uint8_t data)
{



}

void MCP23017::write(uint8_t dataA, uint8_t dataB)
{

    I2CMasterSlaveAddrSet(_baseAddrI2C, _addr, false);

    I2CFIFODataPut(_baseAddrI2C, GPIO_A);
    I2CMasterControl(_baseAddrI2C, I2C_MASTER_CMD_FIFO_BURST_SEND_START);
    I2CFIFODataPut(_baseAddrI2C, dataA);
    I2CFIFODataPut(_baseAddrI2C, dataB);
    I2CMasterControl(_baseAddrI2C, I2C_MASTER_CMD_FIFO_BURST_SEND_FINISH);

}

uint8_t MCP23017::readPort(port rdPort)
{

    return 0;

}

uint16_t MCP23017::read()
{

    return 0;

}

void MCP23017::writeContReg(uint8_t reg, uint8_t data)
{

    I2CMasterSlaveAddrSet(_baseAddrI2C, _addr, false);

    I2CFIFODataPut(_baseAddrI2C, reg);
    I2CMasterControl(_baseAddrI2C, I2C_MASTER_CMD_FIFO_BURST_SEND_START);
    I2CFIFODataPut(_baseAddrI2C, data);
    I2CMasterControl(_baseAddrI2C, I2C_MASTER_CMD_FIFO_BURST_SEND_FINISH);

}

uint8_t MCP23017::readContReg(uint8_t reg)
{

    uint8_t rcvData = 0;

    I2CMasterSlaveAddrSet(_baseAddrI2C, _addr, true);

    return rcvData;

}

void MCP23017::interruptConfig(port intPort, pins intPins, intMode mode, bool mirror)
{



}

void MCP23017::disableInterrupt(port intPort, pins intPins)
{



}

void MCP23017::enableInterrupt(port intPort, pins intPins)
{



}

void MCP23017::clearInterrupt(port intPort, pins intPins)
{



}
