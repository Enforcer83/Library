#ifndef _BLINKER_
#define _BLINKER_

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"

#include "macroFns.h"

class Blink
{
	
private:

	uint32_t ledPort;
	uint32_t ledPin;
	uint32_t onTime;
	uint32_t offTime;
	
	uint32_t previousMillis;
	
	bool stateOn;
	
public:

	Blink(uint32_t periph, uint32_t port, uint32_t pin, uint32_t on, uint32_t off);
    void Update();

};

#endif
