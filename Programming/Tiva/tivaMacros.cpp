#include "tivaMacros.h"

void isrSysTick() {
    
    MICRO_SECOND++;
    
    if (MICRO_SECOND % 1000 == 0) {
    
        MILLI_SECOND++;
        
    }
    
}

bool initSysTick() {

    SysTickPeriodSet(SysCtlClockGet()/1000000);
    
    SysTickIntRegister(isrSysTick);
    
    SysTickIntEnable();
    
    SysTickEnable();
    
    return true;

}

uint32_t micros() {

    return MICRO_SECOND;
    
}

uint32_t millis() {

    return MILLI_SECOND;
    
}
