#ifndef __TIVA_MACROS_H__
#define __TIVA_MACROS_H__

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/interrupt.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"

static volatile uint32_t MICRO_SECOND = 0, MILLI_SECOND = 0;

void isrSysTick();
bool initSysTick();
uint32_t micros();
uint32_t millis();

#endif
