# Library
Schematic symbol and component package libraries for various Circuit Board CAD programs I have used in the past, are learning to use or currently using.

Programs:
* EagleCAD by Autodesk
* PCBArtist by Advanced Circuits
* KiCAD
